from datetime import datetime
import sqlite3
import sys

#create a new connection
connection = sqlite3.connect("ticktacktoe2.db")
cursor = connection.cursor()

#create sqlite database of game data
try:
    cursor.execute("CREATE TABLE Players (Username VARCHAR(100) PRIMARY KEY NOT NULL, games_played INT, games_won INT, games_lost INT, games_drawn INT)")
except:
    pass
try:
    cursor.execute("CREATE TABLE Games (game_number INTEGER PRIMARY KEY, player_1_username VARCHAR(100) NOT NULL, player_2_username VARCHAR(100) NOT NULL, Score VARCHAR(10) NOT NULL, CONSTRAINT FK_player_1 FOREIGN KEY (player_1_username) REFERENCES Players (Username), CONSTRAINT FK_player_2 FOREIGN KEY (player_2_username) REFERENCES Players (Username))")
except:
    pass

# commits these changes
connection.commit()




#create the class Noughts and Crosses
class NoughtsAndCrosses:

    # define the winning indexes.
    winning_indexes = (
        # Horizontals
        (0,1,2),
        (3,4,5),
        (6,7,8),
        
        # Verticals
        (0,3,6),
        (1,4,7),
        (2,5,8),
        
        # Diagonals
        (0,4,8),
        (2,4,6)
    )
    
    
    
    def __init__(self):
        '''
        Initialise the class instance, setting the metrics to 0. 
        '''
        self.player_1 = 'x'
        self.player_2 = 'o'
        self.rounds_played = 0
        self.curr_player = self.player_1 # alternates between x and o
        self.player_1_wins = 0
        self.player_2_wins = 0
        self.player_1_loss = 0
        self.player_2_loss = 0
        self.players_draw = 0
        
        print('''Welcome to Noughts and Crosses!
                 
         (NB. You can enter 'quit' into the input bar to quit the game at any point)
              
              ''')

    
    
    def create_players(self):
        '''
        Creates player 1 and 2, and calls the check_exist method.
        '''
        self.player_1_name = input('Player 1, please enter your name:')
        self.check_exists(self.player_1_name)
        self.player_2_name = input('Player 2, please enter your name:')
        self.check_exists(self.player_2_name)
        
        
    def check_exists(self, username):
        '''
        Checks if the player's usernames already exitst, and if not, it inserts it into the Players table.
        '''
        connection = sqlite3.connect("ticktacktoe2.db")
        cursor = connection.cursor()
        cursor.execute('SELECT username FROM Players')
        names = cursor.fetchall()
        self.quit_game(username)
        if (username,) in names:
            print(f'Welcome back {username}')
        else:
            try:
                cursor.execute("INSERT INTO Players (username, games_won, games_played, games_lost, games_drawn) VALUES (?,0,0,0,0) ", (username,))
                connection.commit()
            except:
                pass
            print(f'Welcome to Noughts and Crosses {username}')
    
    
    def reset_game(self):
        print(
            f'''
            
                Welcome {self.player_1_name} and {self.player_2_name}, lets play Noughts and Crosses!
    ------------------------------------------------------------------------------------------------------------
                  
                  RULES:
                  
                  1. Noughts and crosses is a two player game.
                  2. Player 1 is assigned to crosses: x, and Player 2 is assigned to noughts: o. 
                  3. Players take it in turns to choose a position by typing a number between 0 and 8, 
                     according to the grid below:
                  
                                            {([6,7,8])}
                                            {([3,4,5])}
                                            {([0,1,2])}
                  
                  4. To quit the game at any point, type 'quit' as your move. Your gameplay data will not be stored.
                  5. Enjoy playing! You may choose to continue playing after each round.
                
                ''')
        self.board       = [None]*9
        self.game_over   = False
        self.game_won = False


        

            
    def render(self):
        '''
        Renders the state of the board.
        '''
        print('''
        ''')
        print(self.board[6:9])
        print(self.board[3:6])
        print(self.board[0:3])
        print('''
        ''')
        
        
    def check_win(self):
        '''
        Sets the game to be over when the current player has won, and calls the update games table method.
        '''
        for pos1, pos2, pos3 in NoughtsAndCrosses.winning_indexes:
            if((self.board[pos1] == self.curr_player) and 
               (self.board[pos2] == self.curr_player) and
               (self.board[pos3] == self.curr_player)):
                self.game_won = True
                if self.curr_player == self.player_1:
                    print(f'''
                    
                    {self.player_1_name}, wins!!
                    
                    ''')
                    self.render()
                    self.player_1_wins += 1
                    self.player_2_loss += 1
                else:
                    print(f'''
                    
                    {self.player_2_name}, wins!!
                    
                    ''')
                    self.render()
                    self.player_2_wins += 1 
                    self.player_1_loss += 1
                self.update_games_table()
                self.game_over = True
                self.rounds_played += 1
                self.replay()
        
    def check_filled(self):
        '''
        Sets the game to be over when the board is filled.
        '''
        num_empty_positions = len([i for i in self.board if i is None])
        if num_empty_positions == 0:
            print('Board filled.')
            self.game_over = True
            if self.game_won==False:
                print('''
                It's a draw! 
                ''')
                self.update_games_table()
                self.rounds_played += 1
                self.players_draw += 1
                self.replay()
            
        
    def make_move(self, position):
        '''
        Places a piece for the current player in the specified position.
        '''
        # If the board position is empty, then place our piece.
        if self.board[position] is None:
            self.board[position] = self.curr_player
        else:
            print('Illegal move. Please try again...')
        
        # Check if the game has ended
        self.check_win()
        self.check_filled()
        
        # Switch player
        if self.curr_player is self.player_1:
            self.curr_player = self.player_2
        else:
            self.curr_player = self.player_1
            
    def play(self):
        '''
        Plays the game until game over, allowing players to input their positions
        '''
        self.reset_game()
        self.render()
        while not self.game_over:
            if self.curr_player == self.player_1:
                proposed_position = input(f'Enter a location for {self.player_1_name}:')
            else:
                proposed_position = input(f'Enter a location for {self.player_2_name}:')
            self.quit_game(proposed_position)
            if proposed_position in ['0','1','2','3','4','5','6','7','8']: 
                proposed_position = int(proposed_position)
                self.make_move(proposed_position)
                self.render()
            else:
                print('Invalid move, please try again.')

    def replay(self):
        '''
        allows the players to chose whether to play again
        '''
        self.play_again = input('Play another game? (Y/N)')
        self.quit_game(self.play_again)
        if self.play_again == 'Y':
            self.curr_player = self.player_1 # alternates between x and o
            print('''
            
            Let's play again!
            
            ''')
            self.play()
        elif self.play_again == 'N':
            self.update_players_table()
            sys.exit('''Thank you for playing!
            
            Goodbye!''')
        else:
            print('Invalid input, please enter Y or N.')
            self.replay()
        

    
    def update_games_table(self):
        connection = sqlite3.connect("ticktacktoe2.db")
        cursor = connection.cursor()
        if self.game_won == True:
            cursor.execute("INSERT INTO Games (player_1_username, player_2_username, score) VALUES (?,?,'WIN') ", (self.player_1_name,self.player_2_name))
        elif self.game_won == False:
            cursor.execute("INSERT INTO Games (player_1_username, player_2_username, score) VALUES (?,?,'DRAW') ", (self.player_1_name,self.player_2_name))
        connection.commit()
        connection.close()
    
    def update_players_table(self):
        connection = sqlite3.connect("ticktacktoe2.db")
        cursor = connection.cursor()
        cursor.execute("UPDATE Players SET games_won = coalesce(games_won,0) + coalesce(?,0), games_played = coalesce(games_played,0) + coalesce(?,0), games_lost = coalesce(games_lost,0) + coalesce(?,0), games_drawn = coalesce(games_drawn,0) + coalesce(?,0) WHERE username LIKE ?", (self.player_1_wins, self.rounds_played, self.player_1_loss, self.players_draw, self.player_1_name))
        cursor.execute("UPDATE Players SET games_won = coalesce(games_won,0) + coalesce(?,0), games_played = coalesce(games_played,0) + coalesce(?,0), games_lost = coalesce(games_lost,0) + coalesce(?,0), games_drawn = coalesce(games_drawn,0) + coalesce(?,0) WHERE username LIKE ?", (self.player_2_wins, self.rounds_played, self.player_2_loss, self.players_draw, self.player_2_name))
        connection.commit()
        connection.close()
        
    def quit_game(self, input):
        if input == 'quit':
            self.game_over = True
            sys.exit('Goodbye!')