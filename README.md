# Noughts and Crosses

Noughts and crosses game for Credit Suisse

## Introduction to the Game

This git repository contains the files needed to play Noughts and Crosses and a dashboard containing information about the game.

## Description of Files

**README.md**                       -  This file, containing instructions on how to play the game.

**Playing Noughts and Crosses!.py** - A python file that runs the noughts and crosses game. 

**Noughts and Crosses dashboard.twb**  - A Tableau dashboard containing information about the game.

**ticktacktoe2.db**                     - A SQLite database containing data gathered through user gameplay. This database contains two tables: Players and Games.

**Noughts_and_Crosses dashboard.twb Files**: Images needed to create the Tableau Dashboard.


**Noughts_and_Crosses package**:

- **__init__.py**             - file that initialises the creation of the package

- **play.py**                 - module containing the python code needed to run the two main Noughts and Crosses methods: create_player and play.

- **noughts_and_crosses.py** - module which contains the class NoughtsAndCrosses. This class contains all the methods needed to create and run the noughts and crosses game. 
  

## How to play:

1. Clone this git repository to your chosen destination file.

2. Open the **Noughts and Crosses dashboard.twb** file, navigate to *data source*. 
   
   Under Connections, right click on *SQLite3 ODBC Driver (ODBC)*, and select *edit connection*.

   Replace the *Database* filepath from *C:\Users\CharlotteCoop\OneDrive - Kubrick Group\Python\noughts-and-crosses\ticktacktoe2.db*, to the filepath for your cloned ticktacktoe2.db (e.g. *your file path*\noughts-and-crosses\ticktacktoe2.db).

   Click *sign in* in the bottom right hand corner to commit this change.

3. Open the **Playing Noughts and Crosses!.py** file and follow the instructions to play the game!

4. Refresh your Data Source in **Noughts and Crosses dashboard.twb** to see the dashboard update with your gameplay information. 

(NB. If we were to publish this dashboard to Tableau Online, we would have the capability to automatically update the data without having to refresh the data source. 
However, as we are currently working within local repositories, we must manually update the filepath and refresh the source data.)






