# Welcome to the Noughts and Crosses Game File!
# Run all the code in this file to play:

# Firstly, import the module play from the Noughts_and_Crosses package:
from Noughts_and_Crosses import play


#Then, run the function play_noughts_and_crosses to play the game:
play.play_noughts_and_crosses()